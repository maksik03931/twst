<?php

// Find VirtualHost name and define as variable

$url = $_SERVER['SERVER_NAME'];

// Define multi-store websites url
$website1 = "uzooka-dev.magecloud.net";
$website2 = "headlightsdepot-dev.magecloud.net";

// Test for requested website then display relevant robots.txt content

if (strpos($url,$website1) !== false) {
    header("Content-Type: text/plain");
    header("Content-Length: ". filesize('robots/robots.txt'));
    readfile('robots/robots.txt');
//    include 'robots/robots.txt';
}

if (strpos($url,$website2) !== false) {
    header("Content-Type: text/plain");
    header("Content-Length: ". filesize('robots/robots-hd.txt'));
    readfile('robots/robots-hd.txt');
//    include 'robots/robots-hd.txt';
}

?>